# Anime Guesser Poster-Scrapper

Scrapper utilisé pour récupérer les affiches publicitaires des animés constituant le dataset.
Affiches récupérée sur [MyAnimeList](https://myanimelist.net/) grâce à cette [API](https://jikan.moe/).

## Installation

````shell script
npm install
````

## Utilisation

```
npm start
```

Les affiches sont téléchargées dans le dossier indiqué dans le champ "root" du fichier ``conf.json``
