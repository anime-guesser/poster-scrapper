export interface Pages {
  needed: number;
  total: number;
}

export interface Folders {
  training: string;
  testing: string;
}
