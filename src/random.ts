import {Pages} from "./interface/application";

export function randomUniqueFromArray<T>(arr: T[], num: number): T[] {
  return shuffle<T>(arr).slice(0, num);
}

export function shuffle<T>(arr: T[]): T[] {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }

  return arr;
}

export function randomPage(pages: Pages): number[] {
  const arr = [...Array(pages.total).keys()].map(x => ++x);
  return randomUniqueFromArray<number>(arr, pages.needed);
}


