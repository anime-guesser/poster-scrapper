// Imports
import got from "got";
import PMemoize from "p-memoize";
import PQueue from "p-queue";
import pino from "pino";
import LRU from "quick-lru";

// Constants
export const baseUrl = "https://api.jikan.moe/v3";
export const queue = new PQueue({ concurrency: 2 });

// Custom http client
const http = got.extend({
  baseUrl,
  headers: {
    "User-Agent": 'malearning / 1.0.0 luis@valdez.fr'
  },
  json: true
});

// Memoized http client
export const api = PMemoize(http, { cache: new LRU({ maxSize: 1000 }) });

// Fast JSON logger
export const Logger = pino({
  name: "jikants",
  prettyPrint: true
});
