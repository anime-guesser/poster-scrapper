import * as config from './conf.json';
import * as fs from 'fs';
import rimraf from 'rimraf';
import generator from "./generator";

const root = `${config.root}/${config.folderName}`;
const trainingFolder = `${root}/${config.trainingFolder}`;
const testingFolder = `${root}/${config.testingFolder}`;

if (!fs.existsSync(config.root)) throw Error("Le dossier root n'existe pas");
if (fs.existsSync(root)) rimraf.sync(root);
fs.mkdirSync(root);
fs.mkdirSync(trainingFolder);
fs.mkdirSync(testingFolder);

config.categories.forEach(category => {
  fs.mkdirSync(trainingFolder+ '/' + category.name);
  fs.mkdirSync(testingFolder + '/' + category.name);

  const folders = {
    training: trainingFolder + '/' + category.name,
    testing: testingFolder + '/' + category.name
  }

  generator(folders, config.resultByCategories, category);
});
