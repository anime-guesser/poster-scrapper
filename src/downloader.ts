import request from 'request';
import * as fs from 'fs';
import {AnimeElement} from "./jikan/interfaces/genre/Genre";

export function download(uri: string, filename: string, callback: () => void) {
  request.head(uri, function () {
    request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
  });
};

export function downloadPosters(animes: AnimeElement[], folder: string,
                               category: {id: number, name: string}, type: string) {

  let downloaded = 1;
  animes.forEach(anime => {
    download(anime.image_url, getFilename(anime, folder), () => {
      console.log(`${type} -> ${category.name}: ${downloaded++}/${animes.length}`);
    })
  });
}

function getFilename(anime: AnimeElement, folder: string): string {
  const extension = anime.image_url.substr(anime.image_url.lastIndexOf('.') + 1);
  return `${folder}/${anime.mal_id}.${extension}`;
}
