import jikanTS from "./jikan";
import {Folders, Pages} from "./interface/application";
import {randomPage, randomUniqueFromArray} from "./random";
import {Anime, AnimeElement} from "./jikan/interfaces/genre/Genre";
import {downloadPosters} from "./downloader";

export default async function (folders: Folders, resultNumber: number, category: { name: string, id: number, max: number }) {
  const animes = await getAnimes(resultNumber * 2, category);
  downloadPosters(animes.slice(0, resultNumber), folders.training, category, 'training');
  downloadPosters(animes.slice(resultNumber), folders.testing, category, 'testing');
}

async function getAnimes(resultNumber: number, category: { name: string, id: number, max: number }): Promise<AnimeElement[]> {
  const pageNumber: Pages = await getPageNeeded(category, resultNumber);
  const selectedPages = randomPage(pageNumber);
  let animes: AnimeElement[] = [];
  let pageIdx = 1;

  for (const page of selectedPages) {
    console.log(`${category.name}: loading page ${page} (${pageIdx++}/${selectedPages.length})`)
    const res: Anime | undefined = await jikanTS.Genre.anime(category.id, page);
    if( !res || !res.anime) throw Error("get specific page fail");
    animes.push(...res.anime);
  }

  return randomUniqueFromArray<AnimeElement>(animes, resultNumber);
}

async function getPageNeeded(category: {name: string, id: number, max: number}, resultNumber: number): Promise<Pages> {
  const result = await jikanTS.Genre.anime(category.id);
  if( !result || !result.item_count ) throw Error(category.name + " doesn't exist");
  if( resultNumber > result.item_count )
    throw Error("Too many resultByCategories for " + category.name + ". max: " + Math.floor(result.item_count / 2));

  const total = getPages(result.item_count);

  const max = Math.min(total, category.max);

  const needed = getPages(resultNumber);

  return {needed: Math.min(needed, max) , total: max};
}

function getPages(elementNumber: number) : number {
  return Math.floor(elementNumber / 100) + (elementNumber % 100 > 0 && elementNumber > 100 ? 1 : 0);
}
